# Architecture Ref. Card 03

Spring Boot Application mit MariaDB Database

## Inbetriebnahme auf eigenem Computer

Projekt herunterladen

```sh
git clone https://gitlab.com/levmei/ref-card-03.git
cd ref-card-03
```

Build image mit Dockerfile
```sh
$ docker build -t ref-card-03:latest .
```

Docker Container laufen lassen
```sh
$ docker run -p 8080:8080 ref-card-03:latest
```

Im Browser ist die App unter der URL http://localhost:8080 erreichbar.
